# public


## Recommendations
To help improve collaboration in PermafrostNet, please consider following these recommendations, which have been adopted from existing best-practices.

### Using Git
* If you are collaborating with others on a project, it is valuable to have a shared understanding on how and when to create branches. There are several common workflows or strategies that have been developed for this purpose. We recommend the GitLab Flow strategy. To read more about what this entails, see [the GitLab branching strategies document](https://learn.gitlab.com/version-control/branching-strategies) (download the file once you open the page to escape the popup that asks for your email).  

### Python projects
* When creating a python project, consider creating a directory structure that is amenable to python packaging. This . Some resources to consider when building your project are:
    - [The official python packaging guide](https://packaging.python.org/tutorials/packaging-projects/)
    - [The python packaging authority's sample project directory](https://github.com/pypa/sampleproject) (you can copy this and use it as a template)
    - [Stack Overflow discussion on best practices for project structures](https://stackoverflow.com/questions/193161/what-is-the-best-project-structure-for-a-python-application)
    - [A blog post with other considerations for releasing your project as open source](https://www.jeffknupp.com/blog/2013/08/16/open-sourcing-a-python-project-the-right-way/)
    